class GameChannel < ApplicationCable::Channel
  def subscribed
    stream_from "game_channel_#{channel_uuid}"
  end

  def unsubscribed
    return unless @match
    @match.reload
    return if @match.ended_at
    @match.ended_at = DateTime.now
    @match.save
    @opponent = @match.players.where('id != ?', @player.id).first unless @opponent
    if @opponent
      @player.result = 2
      @player.save
      @opponent.result = 1
      @opponent.save
      ActionCable.server.broadcast "game_channel_#{@opponent.channel_uuid}", action: 'end_match', result: 1
    else
      @player.result = 0
      @player.save
    end
  end

  def wait_for_opponent
    @match = Match.joins(:players).where(ended_at: nil).group("matches.id").having("count(players.id) = ?", 1).first
    if @match
      @opponent = @match.players.first
      @player = @match.players.create(turn: 2, channel_uuid: channel_uuid)
      ActionCable.server.broadcast "game_channel_#{channel_uuid}", action: 'start_match',
                                   player: { username: "playeṛ#{@player.id}", turn: 2 },
                                   opponent: { username: "player#{@opponent.id}", turn: 1 }
      ActionCable.server.broadcast "game_channel_#{@opponent.channel_uuid}", action: 'start_match',
                                   player: { username: "player#{@opponent.id}", turn: 1 },
                                   opponent: { username: "player#{@player.id}", turn: 2 }
    else
      @match = Match.create
      @player = @match.players.create(turn: 1, channel_uuid: channel_uuid)
    end
  end

  def fill_box(data)
    return unless @match
    @match.reload
    return if @match.ended_at || @match.fills.where(box: data['box']).first || (@player.turn == 1 && @match.fills.count%2 != 0) || (@player.turn == 2 && @match.fills.count%2 == 0)
    @opponent = @match.players.where('id != ?', @player.id).first unless @opponent
    fill = @match.fills.create(player_id: @player.id, box: data['box'])
    if @match.fills.count > 4
      if (@match.fills.where(player_id: @player.id, box: 0).first && ((@match.fills.where(player_id: @player.id, box: 1).first && @match.fills.where(player_id: @player.id, box: 2).first)|| (@match.fills.where(player_id: @player.id, box: 4).first && @match.fills.where(player_id: @player.id, box: 8).first) || (@match.fills.where(player_id: @player.id, box: 3).first && @match.fills.where(player_id: @player.id, box: 6).first))) || (@match.fills.where(player_id: @player.id, box: 1).first && @match.fills.where(player_id: @player.id, box: 4).first && @match.fills.where(player_id: @player.id, box: 7).first) || (@match.fills.where(player_id: @player.id, box: 2).first && @match.fills.where(player_id: @player.id, box: 5).first && @match.fills.where(player_id: @player.id, box: 8).first) || (@match.fills.where(player_id: @player.id, box: 3).first && @match.fills.where(player_id: @player.id, box: 4).first && @match.fills.where(player_id: @player.id, box: 5).first) || (@match.fills.where(player_id: @player.id, box: 6).first && ((@match.fills.where(player_id: @player.id, box: 4).first && @match.fills.where(player_id: @player.id, box: 2).first) || (Fill.where(player_id: @player.id, box: 7).first && @match.fills.where(player_id: @player.id, box: 8).first)))
        @match.ended_at = DateTime.now
        @match.save
        @player.result = 1
        @player.save
        @opponent.result = 2
        @opponent.save
        ActionCable.server.broadcast "game_channel_#{@opponent.channel_uuid}", action: 'fill_box', box: fill.box, turn: @player.turn
        ActionCable.server.broadcast "game_channel_#{@opponent.channel_uuid}", action: 'end_match', result: 2
        ActionCable.server.broadcast "game_channel_#{@player.channel_uuid}", action: 'fill_box', box: fill.box, turn: @player.turn
        ActionCable.server.broadcast "game_channel_#{@player.channel_uuid}", action: 'end_match', result: 1
        @match = @player = @opponent = nil
      elsif Fill.where(match_id: @match.id).count >= 9
        @match.ended_at = DateTime.now
        @match.save
        @player.result = 0
        @player.save
        @opponent.result = 0
        @opponent.save
        ActionCable.server.broadcast "game_channel_#{@opponent.channel_uuid}", action: 'fill_box', box: fill.box, turn: @player.turn
        ActionCable.server.broadcast "game_channel_#{@opponent.channel_uuid}", action: 'end_match', result: 0
        ActionCable.server.broadcast "game_channel_#{@player.channel_uuid}", action: 'fill_box', box: fill.box, turn: @player.turn
        ActionCable.server.broadcast "game_channel_#{@player.channel_uuid}", action: 'end_match', result: 0
        @match = @player = @opponent = nil
      else
        ActionCable.server.broadcast "game_channel_#{@opponent.channel_uuid}", action: 'fill_box', box: fill.box, turn: @player.turn
        ActionCable.server.broadcast "game_channel_#{@player.channel_uuid}", action: 'fill_box', box: fill.box, turn: @player.turn
      end
    else
      ActionCable.server.broadcast "game_channel_#{@opponent.channel_uuid}", action: 'fill_box', box: fill.box, turn: @player.turn
      ActionCable.server.broadcast "game_channel_#{@player.channel_uuid}", action: 'fill_box', box: fill.box, turn: @player.turn
    end
  end

  def leave_match
    return unless @match
    @match.reload
    return if @match.ended_at
    @match.ended_at = DateTime.now
    @match.save
    @opponent = @match.players.where('id != ?', @player.id).first unless @opponent
    if @opponent
      @player.result = 2
      @player.save
      @opponent.result = 1
      @opponent.save
      ActionCable.server.broadcast "game_channel_#{@opponent.channel_uuid}", action: 'end_match', result: 1
      ActionCable.server.broadcast "game_channel_#{@player.channel_uuid}", action: 'end_match', result: 2
    else
      @player.result = 0
      @player.save
    end
    @match = @player = @opponent = nil
  end
end
