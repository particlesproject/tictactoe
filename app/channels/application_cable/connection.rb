module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :channel_uuid

    def connect
      self.channel_uuid = cookies[:channel_uuid]
    end
  end
end
