class HomeController < ApplicationController
  def index
    reset_session
    cookies[:channel_uuid] = SecureRandom.uuid
  end
end