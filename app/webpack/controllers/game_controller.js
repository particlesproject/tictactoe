import { Controller } from 'stimulus'
import ActionCable from 'actioncable'

export default class extends Controller {
  static targets = ['menucontainer', 'menu', 'player', 'opponent', 'leave', 'box']

  connect() {
    this.cable = ActionCable.createConsumer('ws://' + location.hostname + ':' + location.port + '/cable')
    this.game = this.cable.subscriptions.create('GameChannel', {
      connected: () => { },

      disconnected: () => { },

      received: (data) => {
        switch(data.action){
        case 'start_match':
          this.start_match(data.player, data.opponent)
          break
        case 'fill_box':
          this.fill_box(data.box, data.turn)
          break
        case 'end_match':
          this.end_match(data.result)
        }
      }
    })
  }

  wait_for_opponent() {
    this.menuTarget.style.visibility = 'hidden'
    this.menuTarget.style.opacity = 0

    this.game.perform('wait_for_opponent')
  }

  start_match(player, opponent){
    this.my_turn = (player.turn == 1 ? true : false)
    
    this.menucontainerTarget.style.visibility = 'hidden'
    this.menucontainerTarget.style.opacity = 0
    
    this.opponentTarget.innerHTML = '<b>' + opponent.username + '</b> | play with the ' + (opponent.turn == 1 ? '<b>X</b>' : '<b>0</b>')
    this.playerTarget.innerHTML = '<b>' + player.username + '</b> | play with the ' + (player.turn == 1 ? '<b>X</b>' : '<b>0</b>')
    // $('#leave').html('<a href="javascript:leaveMatch()">Leave match</a>')
  }

  player_fill(box){
    if(this.my_turn){
      this.game.perform('fill_box', { box: box })
    }
    else
      alert('Not your turn.')
  }

  fill_box_0(){
    this.player_fill(0)
  }

  fill_box_1(){
    this.player_fill(1)
  }

  fill_box_2(){
    this.player_fill(2)
  }

  fill_box_3(){
    this.player_fill(3)
  }

  fill_box_4(){
    this.player_fill(4)
  }

  fill_box_5(){
    this.player_fill(5)
  }

  fill_box_6(){
    this.player_fill(6)
  }

  fill_box_7(){
    this.player_fill(7)
  }

  fill_box_8(){
    this.player_fill(8)
  }

  fill_box(box, turn){
    var element = this.boxTargets[parseInt(box)]

    if(turn == 1)
      element.className = 'cross'
    else
      element.className = 'circle'
    // element.onclick = ''

    if(this.my_turn)
      this.my_turn = false
    else
      this.my_turn = true
  }
  
  leave_match(){
    if(confirm('Are you sure you want to leave this match?')){
      this.game.perform('leave_match')
    }
  }
  
  end_match(result){
    switch(result){
      case 1:
        alert('Has ganado :D')
        break
      case 2:
        alert('Has perdido :(')
        break
      case 0:
        alert('juego empatado :/')
        break
    }

    this.menucontainerTarget.style.visibility = 'visible'
    this.menucontainerTarget.style.opacity = 1
    this.opponentTarget.innerHTML = ''
    this.playerTarget.innerHTML = ''
    // $('#leave').html('')
    this.menuTarget.style.visibility = 'visible'
    this.menuTarget.style.opacity = 1

    this.boxTargets.forEach(element => {
      element.className = ''
    })
  }
}
