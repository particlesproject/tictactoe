class CreateTables < ActiveRecord::Migration[5.1]
  def change
    create_table :matches do |t|
      t.datetime :ended_at, null: true
      t.timestamps
    end

    create_table :players do |t|
      t.string   :channel_uuid
      t.references :match, foreign_key: true, null: true
      t.integer  :turn
      t.integer  :result
      t.timestamps
    end

    create_table :fills do |t|
      t.references :match, foreign_key: true
      t.references :player, foreign_key: true
      t.integer :box
      t.timestamps
    end
  end
end